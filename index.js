'use strict';
module.exports = function (app) {
    app.get('/', function (req, res) {
        res.render('index');
    });
    app.get('/general-home-page', function (req, res) {
        res.render('general-home-page');
    });
    app.get('/about-page', function (req, res) {
        res.render('about-page');
    });
    app.get('/service-page', function (req, res) {
        res.render('service-page');
    });
    app.get('/team-page', function (req, res) {
        res.render('team-page');
    });
    app.get('/demo-template-page', function (req, res) {
        res.render('demo-template-page');
    });
    app.get('/portfolio-page', function (req, res) {
        res.render('portfolio-page');
    });
    app.get('/portfolio-single-page', function (req, res) {
        res.render('portfolio-single-page');
    });
    app.get('/contact-page', function (req, res) {
        res.render('contact-page');
    });
    app.get('/shop-page', function (req, res) {
        res.render('shop-page');
    });
    app.get('/product-page', function (req, res) {
        res.render('product-page');
    });
    app.get('/cart-page', function (req, res) {
        res.render('cart-page');
    });
    app.get('/checkout-page', function (req, res) {
        res.render('checkout-page');
    });
    app.get('/blog-page', function (req, res) {
        res.render('blog-page');
    });
    app.get('/blog-single-page', function (req, res) {
        res.render('blog-single-page');
    });
};